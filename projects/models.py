from django.db import models
from django.conf import settings

USER_MODEL = settings.AUTH_USER_MODEL

# Create your models here.


class Project(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()
    project_image = models.ImageField(
        null=True, blank=True, upload_to="images/"
    )
    members = models.ManyToManyField(
        USER_MODEL,
        related_name="projects",
    )

    def __str__(self):
        return self.name
