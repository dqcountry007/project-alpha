# App name: Project Tracker 

## Overview

* Project Tracker is a project & task management application for anyone who is looking to keep their daily tasks organized, in addition to dynamically updating the details of the project as it progresses. Users are also able to login, logout, and sign up for the application.

## Technologies

* Python
* Django
    * Django authentication system
    * Generic class-based display and editing views 
* SQLite 3
* HTML
* CSS

## Screenshots

* ![Login and Sign Up screen](media/images/screenshots/Project_Tracker_login_and_signup.png)
* ![Project form and Task Form](media/images/screenshots/Add_a_project_form_and_Create_task_form.png)
* ![Kitchen project with tasks](media/images/screenshots/My_Tasks_and_Kitchen_project_open.png)
* ![Notes and Update task form](media/images/screenshots/Notes_and_Update_task_form_for_kitchen.png)
* ![Additional projects with project details](media/images/screenshots/Project_list_and_projects_with_tasks.png)


## How to clone and run this application

* 1.) Visit the Project Alpha repository at https://gitlab.com/dqcountry007/project-alpha 
* 2.) Click on the button labeled, "Clone" that has the dropdown icon next to it. 
* 3.) Copy the url under the "Clone with HTTPS". 
* 4.) Open your terminal and change into a directory where you want to store this application. 
* 5.) In your terminal, type "git clone" then paste the HTTPS url you copied and hit enter on your keyboard.
* 6.) Now the Project Alpha repository is cloned locally to your machine. 
* 7.) Change directories into the Project Alpha directory. 
* 8.) You will need to create a virtual environment to run Project alpha by typing in "python -m venv .venv" (no quotations) into your terminal.
* 9.) Activate the virtual environment by typing in "source ./.venv/bin/activate" (no quotations) into your terminal. 
* 10.) You may or may not need to update your package installer for Python (pip). To do this, type "python -m pip install --upgrade pip" (no quotations) into your terminal. 
* 11.) Run your development server by typing in "python manage.py runserver" (no quotations) in your terminal. 
* 12.) In your browser, go to localhost:8000 and you will be brought to the log in screen of Project Alpha, where you can login or signup to use the application. 


