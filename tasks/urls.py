from django.urls import path

from tasks.views import (
    TaskCreateView,
    TaskListView,
    TaskUpdateView,
    TaskFieldsUpdateView,
    TaskDetailView,
)

urlpatterns = [
    path("create/", TaskCreateView.as_view(), name="create_task"),
    path("mine/", TaskListView.as_view(), name="show_my_tasks"),
    path("<int:pk>/complete/", TaskUpdateView.as_view(), name="complete_task"),
    path("<int:pk>/taskupdate/", TaskFieldsUpdateView.as_view(), name="update_task"),
    path("<int:pk>/tasknotes/", TaskDetailView.as_view(), name="show_notes"),
]
